nq (1.0-0.2) unstable; urgency=medium

  *  Replace /usr/bin/fq by /usr/bin/nqtail in debian/tests.

 -- Hilmar Preuße <hille42@debian.org>  Mon, 22 Jul 2024 12:31:00 +0200

nq (1.0-0.1) unstable; urgency=medium

  * Non-maintainer upload.

  * New upstream version (Closes: #1038937).
    - Incompatible change: The fq utility has been renamed to nqtail.
    - Incompatible change: The tq utility has been renamed to nqterm.
    This should solve the file conflict with the fq package
    (Closes: #1005961).

  * Bump Standards and dh compat version, no changes needed.
  * Add upstream metadata file.

  [ Christoph Biedl ]
  * Add autopkg test.

 -- Hilmar Preuße <hille42@debian.org>  Sat, 06 Jul 2024 15:34:34 +0200

nq (0.3.1-4) unstable; urgency=medium

  [ Helmut Grohne ]
  * Fix FTCBFS: Let dh_auto_build pass cross tools to make
    Closes: #951530

  [ nicoo ]
  * Setup Salsa CI
  * d/changelog: Update nicoo's name and address
  * d/watch: Use upstream's canonical repository, at vuxu.org
  * d/copyright: Refer to upstream's canonical repo at vuxu.org
  * d/source: Drop unused lintian-overrides

 -- nicoo <nicoo@debian.org>  Mon, 02 Nov 2020 11:49:55 +0100

nq (0.3.1-3) unstable; urgency=medium

  * Upstream repository relocated to https://github.com/leahneukirchen
  * Update upstream's signing key
  * Update authorship information

  * debian/control
    + Update Homepage
    + debian/control: Declare compliance with policy v4.5.0.
    + debian/control: Declare compliance with policy v4.5.0.
      No change needed.

  * debian/gbp.conf
    + Move packaging branch to debian/sid, use DEP14
    + Fixup pristine-tar usage

  * debian/rules
    + Honor build flags (incl. hardening)
    + Fail build on dh_missing
    + Update to uscan v4, use git mode, check tag signature

 -- nicoo <nicoo@debian.org>  Sat, 15 Feb 2020 23:23:27 +0100

nq (0.3.1-2) unstable; urgency=low

  * Switch to debhelper 12.
    The compatibility level is now controlled through a Build-Depends

  * debian/gbp.conf
    + Commit upstream tarballs to pristine-tar.
    + Compress tarballs with xz.

  * debian/control
    + Comply with policy 4.3.0
      Set Rules-Requires-Root to no
    + Update Maintainer's email address

  * debian/copyright: Update Debian maintainer's copyright line

 -- nicoo <nicoo@debian.org>  Mon, 04 Mar 2019 21:55:59 +0100

nq (0.3.1-1) unstable; urgency=medium

  * New upstream version (2018-03-07)
  * Use dh_installchangelogs rather than dh_exec
  * Migrate the packaging repository to salsa.d.o

 -- nicoo <nicoo@debian.org>  Sat, 24 Mar 2018 00:24:56 +0100

nq (0.2.2-2) unstable; urgency=medium

  * Bump Standards-Version to 4.1.3
    * Update debian/copyright.
      Refer to the CC0-1.0 license file under /usr/share/common-licenses
    * Use an HTTPS format URI

  * Lintian overrides
    * Relocate to debian/source/
    * Rename debian-watch-may-check-gpg-signature

  * Ship upstream's changelog
  * Switch to debhelper 11
  * debian/rules: Remove spurious --parallel

 -- nicoo <nicoo@debian.org>  Tue, 13 Feb 2018 20:58:01 +0100

nq (0.2.2-1) unstable; urgency=medium

  * New upstream version (2017-12-21)
    * Fix an infinite loop in fq when `$NQDIR` is set and inotify is used.
      Thanks to Sebastian Reuße.
  * debian/changelog: Declared compliance with policy 4.1.2
    * No change needed
    * 4.1.3 compliance pending a base-files update containing the CC0 license
  * Silenced the debian-watch-could-verify-download Lintian warning
    Upstream releases signed git tags, and those are the signatures we check.

 -- nicoo <nicoo@debian.org>  Fri, 05 Jan 2018 23:25:05 +0100

nq (0.2.1-1) unstable; urgency=medium

  * Initial packaging (Closes: #882567)

 -- nicoo <nicoo@debian.org>  Fri, 24 Nov 2017 02:16:47 +0100
